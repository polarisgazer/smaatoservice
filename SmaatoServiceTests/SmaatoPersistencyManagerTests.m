//
//  SmaatoPersistenceManagerTests.m
//  SmaatoServiceTests
//
//  Created by Cristian Azov on 26.04.21.
//

#import <XCTest/XCTest.h>
#import "SmaatoPostsProvider.h"
#import "SmaatoPersistenceManager.h"
#import "SmaatoPost.h"
#import "SmaatoUser.h"

@interface SmaatoPersistenceManagerTests : XCTestCase

@end

@implementation SmaatoPersistenceManagerTests

- (void)tearDown {
    NSError *error = nil;
    [[[SmaatoPersistenceManager alloc] init] removeAllDataWithError:&error];
    NSAssert(error == nil, error.localizedDescription);
}

- (void)testPersistence {

    SmaatoPersistenceManager *persistenceManager = [[SmaatoPersistenceManager alloc] init];

    NSError *error = nil;

    NSArray *savedPosts = [persistenceManager retrievePostsWithError:&error];
    XCTAssertNil(error);
    XCTAssertNil(savedPosts);

    SmaatoPost *post1 = [[SmaatoPost alloc] initWithDictionary:@{@"user": @{@"name": @"post1"}}];
    SmaatoPost *post2 = [[SmaatoPost alloc] initWithDictionary:@{@"user": @{@"name": @"post2"}}];
    SmaatoPost *post3 = [[SmaatoPost alloc] initWithDictionary:@{@"user": @{@"name": @"post3"}}];
    SmaatoPost *favoritePost4 = [[SmaatoPost alloc] initWithDictionary:@{@"user": @{@"name": @"favoritePost4"}}];
    SmaatoPost *favoritePost5 = [[SmaatoPost alloc] initWithDictionary:@{@"user": @{@"name": @"favoritePost5"}}];

    NSArray*usersToStore = @[post1, post2, post3, favoritePost4, favoritePost5];
    [persistenceManager storePosts:usersToStore error:&error];

    XCTAssertNil(error);
    savedPosts = [persistenceManager retrievePostsWithError:&error];

    XCTAssertNil(error);
    XCTAssertTrue([[(SmaatoPost *)savedPosts[0] user].name isEqualToString:@"post1"]);
    XCTAssertTrue([[(SmaatoPost *)savedPosts[1] user].name isEqualToString:@"post2"]);
    XCTAssertTrue([[(SmaatoPost *)savedPosts[2] user].name isEqualToString:@"post3"]);
    XCTAssertTrue([[(SmaatoPost *)savedPosts[3] user].name isEqualToString:@"favoritePost4"]);
    XCTAssertTrue([[(SmaatoPost *)savedPosts[4] user].name isEqualToString:@"favoritePost5"]);

    NSArray *savedFavoriteUsers = [persistenceManager retrieveFavoritePostsWithError:&error];
    XCTAssertNil(error);
    XCTAssertNil(savedFavoriteUsers);

    [persistenceManager addFavoritePost:favoritePost4 error:&error];
    XCTAssertNil(error);

    savedFavoriteUsers = [persistenceManager retrieveFavoritePostsWithError:&error];
    XCTAssertNil(error);
    XCTAssertTrue([[(SmaatoPost *)savedFavoriteUsers[0] user].name isEqualToString:@"favoritePost4"]);

    [persistenceManager addFavoritePost:favoritePost5 error:&error];
    XCTAssertNil(error);

    savedFavoriteUsers = [persistenceManager retrieveFavoritePostsWithError:&error];
    XCTAssertNil(error);
    XCTAssertTrue([[(SmaatoPost *)savedFavoriteUsers[1] user].name isEqualToString:@"favoritePost5"]);
}

@end
