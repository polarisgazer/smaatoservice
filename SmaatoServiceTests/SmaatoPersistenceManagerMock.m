//
//  SmaatoPersistenceManagerMock.m
//  SmaatoServiceTests
//
//  Created by Cristian Azov on 28.04.21.
//

#import "SmaatoPersistenceManagerMock.h"

@implementation SmaatoPersistenceManagerMock

- (instancetype)init
{
    self = [super init];
    if (self) {
        _addFavoritePostCalls = [NSMutableArray array];
    }
    return self;
}

- (BOOL)storePosts:(NSArray<SmaatoPost *> *)posts error:(NSError **)errorPtr {
    return YES;
}

- (nullable NSArray<SmaatoPost *> *)retrievePostsWithError:(NSError **)errorPtr {
    return nil;
}

- (BOOL)addFavoritePost:(SmaatoPost *)post error:(NSError **)errorPtr {
    [_addFavoritePostCalls addObject:post];
    return YES;
}

- (nullable NSArray<SmaatoPost *> *)retrieveFavoritePostsWithError:(NSError **)errorPtr {
    return nil;
}

- (BOOL)removeAllDataWithError:(NSError **)errorPtr {
    return YES;
}

@end
