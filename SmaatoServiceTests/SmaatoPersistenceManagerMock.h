//
//  SmaatoPersistenceManagerMock.h
//  SmaatoServiceTests
//
//  Created by Cristian Azov on 28.04.21.
//

#import <Foundation/Foundation.h>
#import "SmaatoPersistenceManaging.h"

NS_ASSUME_NONNULL_BEGIN

@interface SmaatoPersistenceManagerMock : NSObject <SmaatoPersistenceManaging>
/// Stores calls to `addFavoritePost:error:`
@property (nonatomic, strong, nullable) NSMutableArray<SmaatoPost *> *addFavoritePostCalls;

@end

NS_ASSUME_NONNULL_END
