//
//  SmaatoPostsProviderTests.m
//  SmaatoServiceTests
//
//  Created by Cristian Azov on 27.04.21.
//

#import <XCTest/XCTest.h>
#import "SmaatoPost.h"
#import "SmaatoUser.h"
#import "SmaatoPostsProvider.h"
#import "SmaatoNetworkingProviderMock.h"
#import "SmaatoPersistenceManagerMock.h"

@interface SmaatoPostsProviderTests : XCTestCase

@property (nonatomic, strong) NSData *mockData;

@end

@implementation SmaatoPostsProviderTests

- (void)setUp {
    NSBundle *bundle = [NSBundle bundleForClass:[SmaatoPostsProviderTests class]];
    NSURL *mockURL = [bundle URLForResource:@"usersMock" withExtension:@"json"];
    NSAssert(mockURL != nil, @"'usersMock.json' not found in %@", bundle);

    _mockData = [NSData dataWithContentsOfURL:mockURL];
}

- (void)testFetchRandomPost {

    SmaatoNetworkingProviderMock *networkingProviderMock = [[SmaatoNetworkingProviderMock alloc] init];
    SmaatoPersistenceManagerMock *persistenceProviderMock = [[SmaatoPersistenceManagerMock alloc] init];

    __weak typeof(self) weakSelf = self;
    networkingProviderMock.performRequestMock = ^(SmaatoNetworkingCompletion completion) {
        completion(weakSelf.mockData, nil);
    };

    SmaatoPostsProvider *provider = [[SmaatoPostsProvider alloc] initWithNetworkingProvider:networkingProviderMock
                                                                       persistenceManager:persistenceProviderMock];

    XCTestExpectation *expectation = [self expectationWithDescription:@"expectation"];

    [provider fetchRandomPostWithCompletion:^(SmaatoPost * _Nullable post, NSError * _Nullable error) {
        XCTAssertNotNil(post);
        [expectation fulfill];
    }];

    [self waitForExpectations:@[expectation] timeout:0.1];
}

- (void)testMarkAsFavoriteCall {

    SmaatoNetworkingProviderMock *networkingProviderMock = [[SmaatoNetworkingProviderMock alloc] init];
    SmaatoPersistenceManagerMock *persistenceManagerMock = [[SmaatoPersistenceManagerMock alloc] init];
    SmaatoPostsProvider *provider = [[SmaatoPostsProvider alloc] initWithNetworkingProvider:networkingProviderMock
                                                                       persistenceManager:persistenceManagerMock];

    SmaatoPost *post = [[SmaatoPost alloc] initWithDictionary:@{@"user": @{@"name": @"Spock"}}];

    [provider markAsFavoritePost:post error:nil];
    XCTAssertEqual(persistenceManagerMock.addFavoritePostCalls.count, 1);

    SmaatoPost *favoritePost = persistenceManagerMock.addFavoritePostCalls[0];
    XCTAssertTrue([favoritePost.user.name isEqualToString:@"Spock"]);
}

@end
