//
//  SmaatoPostsProviderMock.m
//  SmaatoServiceTests
//
//  Created by Cristian Azov on 27.04.21.
//

#import "SmaatoPostsProviderMock.h"

@implementation SmaatoPostsProviderMock

- (void)fetchRandomPostWithCompletion:(nonnull PostProviderCompletion)completion {
    _fetchRandomPostWithCompletionMock(completion);
}

- (BOOL)markAsFavoritePost:(nonnull SmaatoPost *)post error:(NSError * _Nullable *)errorPtr {
    return YES;
}

@end
