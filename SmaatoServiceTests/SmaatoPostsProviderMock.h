//
//  SmaatoPostsProviderMock.h
//  SmaatoServiceTests
//
//  Created by Cristian Azov on 27.04.21.
//

#import <Foundation/Foundation.h>
#import "SmaatoPostsProviding.h"
@class SmaatoPost;
NS_ASSUME_NONNULL_BEGIN

@interface SmaatoPostsProviderMock : NSObject <SmaatoPostsProviding>

@property (nonatomic, strong, nullable) void (^fetchRandomPostWithCompletionMock)(_Nullable PostProviderCompletion);

@end

NS_ASSUME_NONNULL_END
