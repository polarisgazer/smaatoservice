//
//  SmaatoNetworkingProviderMock.h
//  SmaatoServiceTests
//
//  Created by Cristian Azov on 27.04.21.
//

#import <Foundation/Foundation.h>
#import "SmaatoNetworkingProviding.h"

@interface SmaatoNetworkingProviderMock : NSObject <SmaatoNetworkingProviding>

@property (nonatomic, copy, nullable) void (^performRequestMock)(_Nonnull SmaatoNetworkingCompletion);

@end
