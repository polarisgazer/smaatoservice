//
//  SmaatoNetworkingProviderMock.m
//  SmaatoServiceTests
//
//  Created by Cristian Azov on 27.04.21.
//

#import "SmaatoNetworkingProviderMock.h"

@implementation SmaatoNetworkingProviderMock

- (void)performRequestAtPath:(nonnull NSString *)path
                  parameters:(nonnull NSDictionary<NSString *,NSString *> *)parameters
                      method:(SmaatoHTTPMethod)method
                  completion:(nonnull SmaatoNetworkingCompletion)completion {
    _performRequestMock(completion);
}

@end
