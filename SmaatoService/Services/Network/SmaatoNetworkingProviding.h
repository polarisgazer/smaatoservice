//
//  SmaatoNetworkingProviding.h
//  SmaatoService
//
//  Created by Cristian Azov on 26.04.21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, SmaatoHTTPMethod) {
    SmaatoHTTPMethodGet
};

typedef void (^SmaatoNetworkingCompletion)(NSData * _Nullable data, NSError * _Nullable error);

@protocol SmaatoNetworkingProviding <NSObject>

// http://private-d847e-demoresponse.apiary-mock.com/questions
- (void)performRequestAtPath:(nonnull NSString *)path
                  parameters:(nonnull NSDictionary<NSString *, NSString *> *)parameters
                      method:(SmaatoHTTPMethod)method
                  completion:(SmaatoNetworkingCompletion)completion;
@end

NS_ASSUME_NONNULL_END
