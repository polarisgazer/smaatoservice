//
//  SmaatoNetworkingProvider.m
//  SmaatoService
//
//  Created by Cristian Azov on 26.04.21.
//

#import "SmaatoNetworkingProvider.h"

@interface SmaatoNetworkingProvider ()

@property (nonatomic, strong) NSURL *baseURL;
@property (nonatomic, strong) NSURLSession *urlSession;

@end

@implementation SmaatoNetworkingProvider

- (instancetype)initWithBaseURL:(nonnull NSURL *)baseURL urlSession:(nonnull NSURLSession *)urlSession {
    self = [super init];
    if (self) {
        _baseURL = baseURL;
        _urlSession = urlSession;
    }
    return self;
}

- (void)performRequestAtPath:(nonnull NSString *)path
                  parameters:(nonnull NSDictionary<NSString *, NSString *> *)parameters
                      method:(SmaatoHTTPMethod)method
                  completion:(SmaatoNetworkingCompletion)completion {

    switch(method) {
        case SmaatoHTTPMethodGet:
            [self getDataAtPath:path completion:completion];
    }

}

typedef void (^URLSessionDataTaskCompletion)(NSData *data, NSURLResponse *response, NSError *error);

- (void)getDataAtPath:(nonnull NSString *)path completion:(SmaatoNetworkingCompletion)completion {

    NSCharacterSet *characterSet = [NSCharacterSet URLPathAllowedCharacterSet];
    NSString *escapedPath = [path stringByAddingPercentEncodingWithAllowedCharacters:characterSet];

    if (escapedPath == nil) {
        completion(nil, [NSError new]);
        return;
    }
    NSURL *url = [[NSURL alloc] initWithString:escapedPath relativeToURL:_baseURL];

    if (url == nil) {
        completion(nil, [NSError new]);
        return;
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";

    [[_urlSession dataTaskWithRequest:request
                    completionHandler:[self handleResponseWithCompletion:completion]] resume];

}

- (URLSessionDataTaskCompletion)handleResponseWithCompletion:(SmaatoNetworkingCompletion)completion {

    return ^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {

        if (error != nil) {
            completion(nil, error);
            return;
        }

        if ([(NSHTTPURLResponse *)response statusCode] != 200) {
            completion(nil, [NSError new]);
            return;
        }

        if (data == nil) {
            completion(nil, [NSError new]);
            return;
        }

        completion(data, nil);
    };
}

@end
