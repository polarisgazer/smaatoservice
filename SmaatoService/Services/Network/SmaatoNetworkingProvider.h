//
//  SmaatoNetworkingProvider.h
//  SmaatoService
//
//  Created by Cristian Azov on 26.04.21.
//

#import <Foundation/Foundation.h>
#import "SmaatoNetworkingProviding.h"

NS_ASSUME_NONNULL_BEGIN

@interface SmaatoNetworkingProvider : NSObject <SmaatoNetworkingProviding>

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithBaseURL:(nonnull NSURL *)baseURL urlSession:(nonnull NSURLSession *)urlSession;

@end

NS_ASSUME_NONNULL_END
