//
//  SmaatoPersistenceManaging.h
//  SmaatoService
//
//  Created by Cristian Azov on 26.04.21.
//

#import <Foundation/Foundation.h>
@class SmaatoPost;

NS_ASSUME_NONNULL_BEGIN

@protocol SmaatoPersistenceManaging <NSObject>

- (BOOL)storePosts:(NSArray<SmaatoPost *> *)posts error:(NSError **)errorPtr;
- (nullable NSArray<SmaatoPost *> *)retrievePostsWithError:(NSError **)errorPtr;

- (BOOL)addFavoritePost:(SmaatoPost *)post error:(NSError **)errorPtr;
- (nullable NSArray<SmaatoPost *> *)retrieveFavoritePostsWithError:(NSError **)errorPtr;

- (BOOL)removeAllDataWithError:(NSError **)errorPtr;

@end

NS_ASSUME_NONNULL_END
