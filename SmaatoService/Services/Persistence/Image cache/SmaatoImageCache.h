//
//  SmaatoImageCache.h
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SmaatoImageCache : NSCache <NSString *, UIImage *>

- (instancetype)initWithCountLimit:(NSUInteger)countLimit andName:(NSString *)name;
- (void)cacheImage:(UIImage *)image withURL:(NSURL *)url;
- (nullable UIImage *)cachedImageWithURL:(nonnull NSURL *)url;

@end

NS_ASSUME_NONNULL_END
