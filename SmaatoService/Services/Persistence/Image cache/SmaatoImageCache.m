//
//  SmaatoImageCache.m
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import "SmaatoImageCache.h"
#import "NSString+SHA256.h"

@implementation SmaatoImageCache

- (instancetype)init {
    return [self initWithCountLimit:20 andName:@"DefaultURLImageCache"];
}

- (instancetype)initWithCountLimit:(NSUInteger)countLimit andName:(NSString *)name {
    self = [super init];
    if (self) {
        self.countLimit = countLimit;
        self.name = name;
    }
    return self;
}

- (void)cacheImage:(UIImage *)image withURL:(NSURL *)url {
    NSString *key = url.absoluteString.sha256;
    [self setObject:image forKey:key];
}

- (nullable UIImage *)cachedImageWithURL:(nonnull NSURL *)url {
    NSString *key = url.absoluteString.sha256;
    return [self objectForKey:key];
}

@end
