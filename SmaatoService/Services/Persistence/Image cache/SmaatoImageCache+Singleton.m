//
//  SmaatoImageCache+Singleton.m
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import "SmaatoImageCache+Singleton.h"

@implementation SmaatoImageCache (Singleton)

+ (instancetype)sharedImageCache {

    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[SmaatoImageCache alloc] init];
    });
    return instance;
}

@end
