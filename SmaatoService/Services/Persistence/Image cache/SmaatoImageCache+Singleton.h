//
//  SmaatoImageCache+Singleton.h
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import "SmaatoImageCache.h"

NS_ASSUME_NONNULL_BEGIN

@interface SmaatoImageCache (Singleton)

+ (instancetype)sharedImageCache;

@end

NS_ASSUME_NONNULL_END
