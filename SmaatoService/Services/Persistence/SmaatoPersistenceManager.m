//
//  SmaatoPersistenceManager.m
//  SmaatoService
//
//  Created by Cristian Azov on 26.04.21.
//

#import "SmaatoPersistenceManager.h"
#import "SmaatoPost.h"

static NSString *const PostsFileName = @"posts";
static NSString *const FavoritePostsFileName = @"favoritePosts";

@implementation SmaatoPersistenceManager

- (BOOL)storePosts:(NSArray<SmaatoPost *> *)posts error:(NSError **)errorPtr {
    NSURL *fileURL = [[self baseFileURL] URLByAppendingPathComponent:PostsFileName];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:posts requiringSecureCoding:NO error:errorPtr];
    [data writeToURL:fileURL options:0 error:errorPtr];
    return YES;
}

- (nullable NSArray<SmaatoPost *> *)retrievePostsWithError:(NSError **)errorPtr {
    return [self retrievePostsFromFile:PostsFileName error:errorPtr];
}

- (BOOL)addFavoritePost:(SmaatoPost *)post error:(NSError **)errorPtr {
    NSArray *savedPosts = [self retrievePostsFromFile:FavoritePostsFileName error:errorPtr];
    NSMutableArray *postsToSave = nil;
    if (savedPosts == nil) {
        postsToSave = [NSMutableArray arrayWithObject:post];
    } else {
        postsToSave = [NSMutableArray arrayWithArray:savedPosts];
        [postsToSave addObject:post];
    }
    NSData *dataToSave = [NSKeyedArchiver archivedDataWithRootObject:postsToSave requiringSecureCoding:NO error:errorPtr];
    NSURL *fileURL = [[self baseFileURL] URLByAppendingPathComponent:FavoritePostsFileName];
    [dataToSave writeToURL:fileURL options:0 error:errorPtr];
    return YES;
}

- (nullable NSArray<SmaatoPost *> *)retrieveFavoritePostsWithError:(NSError **)errorPtr {
    return [self retrievePostsFromFile:FavoritePostsFileName error:errorPtr];
}

- (nullable NSArray<SmaatoPost *> *)retrievePostsFromFile:(NSString *)fileName error:(NSError **)errorPtr {
    NSURL *fileURL = [[self baseFileURL] URLByAppendingPathComponent:fileName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]]) {
        return nil;
    }
    NSData *data = [NSData dataWithContentsOfURL:fileURL options:0 error:errorPtr];
    return [NSKeyedUnarchiver unarchivedArrayOfObjectsOfClass:[SmaatoPost class] fromData:data error:errorPtr];
}

- (nullable NSURL *)baseFileURL {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];
}

- (BOOL)removeAllDataWithError:(NSError **)errorPtr {
    if ([self removeFileWithName:PostsFileName error:errorPtr]) {
        return [self removeFileWithName:FavoritePostsFileName error:errorPtr];
    }
    return NO;
}

- (BOOL)removeFileWithName:(NSString *)fileName error:(NSError **)errorPtr {
    NSURL *fileURL = [[self baseFileURL] URLByAppendingPathComponent:fileName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]]) {
        return NO;
    }
    [[NSFileManager defaultManager] removeItemAtURL:fileURL error:errorPtr];
    return YES;
}

@end
