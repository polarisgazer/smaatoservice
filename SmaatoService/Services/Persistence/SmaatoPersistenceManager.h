//
//  SmaatoPersistenceManager.h
//  SmaatoService
//
//  Created by Cristian Azov on 26.04.21.
//

#import <Foundation/Foundation.h>
#import "SmaatoPersistenceManaging.h"

NS_ASSUME_NONNULL_BEGIN

@interface SmaatoPersistenceManager : NSObject <SmaatoPersistenceManaging>

@end

NS_ASSUME_NONNULL_END
