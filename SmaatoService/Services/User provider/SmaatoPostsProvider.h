//
//  SmaatoPostsProvider.h
//  SmaatoService
//
//  Created by Cristian Azov on 26.04.21.
//

#import <Foundation/Foundation.h>
#import "SmaatoPostsProviding.h"
@protocol SmaatoNetworkingProviding;
@protocol SmaatoPersistenceManaging;

NS_ASSUME_NONNULL_BEGIN

@interface SmaatoPostsProvider : NSObject <SmaatoPostsProviding>

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithNetworkingProvider:(id<SmaatoNetworkingProviding>)networkingProvider
                        persistenceManager:(id<SmaatoPersistenceManaging>)persistenceManager;
@end

NS_ASSUME_NONNULL_END
