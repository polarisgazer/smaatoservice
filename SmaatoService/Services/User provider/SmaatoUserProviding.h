//
//  SmaatoUserProviding.h
//  SmaatoService
//
//  Created by Cristian Azov on 26.04.21.
//

#import <Foundation/Foundation.h>
@class SmaatoPost;

typedef void (^PostProviderCompletion)(SmaatoPost * _Nullable user, NSError * _Nullable error);

@protocol SmaatoPostProviding <NSObject>

- (void)fetchRandomPostWithCompletion:(nonnull PostProviderCompletion)completion;
- (void)makeFavoriteUser:()
@end
