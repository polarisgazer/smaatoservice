//
//  SmaatoPostsProvider.m
//  SmaatoService
//
//  Created by Cristian Azov on 26.04.21.
//

#import "SmaatoPostsProvider.h"
#import "SmaatoNetworkingProviding.h"
#import "SmaatoPersistenceManaging.h"
#import "SmaatoPost.h"

typedef void (^PostsProviderCompletion)(NSArray<SmaatoPost *> * _Nullable posts, NSError * _Nullable error);
static NSString *const PostsProviderEndpointPath = @"questions";

@interface SmaatoPostsProvider ()

@property (nonatomic, strong) id<SmaatoNetworkingProviding> networkingProvider;
@property (nonatomic, strong) id<SmaatoPersistenceManaging> persistenceManager;

@end

@implementation SmaatoPostsProvider

- (instancetype)initWithNetworkingProvider:(id<SmaatoNetworkingProviding>)networkingProvider
                        persistenceManager:(id<SmaatoPersistenceManaging>)persistenceManager
{
    self = [super init];
    if (self) {
        _networkingProvider = networkingProvider;
        _persistenceManager = persistenceManager;
    }
    return self;
}

- (void)fetchRandomPostWithCompletion:(nonnull PostProviderCompletion)completion {

    
    [self fetchPostsWithCompletion:^(NSArray<SmaatoPost *> * _Nullable posts, NSError * _Nullable error) {

        if (error != nil) {
            completion(nil, error);
            return;
        }

        if (posts.count == 0) {
            completion(nil, [NSError new]);
            return;
        }

        if (posts.count == 1) {
            completion(posts.firstObject, nil);
            return;
        }

        NSInteger randomIndex = arc4random_uniform((int)posts.count + 1);
        completion(posts[randomIndex], nil);
    }];
}

- (BOOL)markAsFavoritePost:(nonnull SmaatoPost *)post
                     error:(NSError * _Nullable __autoreleasing * _Nullable)errorPtr {
    return [_persistenceManager addFavoritePost:post error:errorPtr];
}

- (void)fetchPostsWithCompletion:(nonnull PostsProviderCompletion)completion {

    [_networkingProvider performRequestAtPath:PostsProviderEndpointPath
                                parameters:@{}
                                    method:SmaatoHTTPMethodGet
                                completion:^(NSData * _Nullable data, NSError * _Nullable error) {
        if (error != nil) {
            completion(nil, error);
            return;
        }

        if (data == nil) {
            completion(nil, [NSError new]);
            return;
        }

        NSError *jsonError;
        NSArray<NSDictionary<NSString *, NSString *> *> *jsonPosts = [NSJSONSerialization
                                                                      JSONObjectWithData:data
                                                                      options:NSJSONReadingMutableContainers
                                                                      error:&jsonError];
        if (jsonError != nil) {
            completion(nil, jsonError);
            return;
        }

        NSMutableArray *users = [NSMutableArray arrayWithCapacity:jsonPosts.count];

        [jsonPosts enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [users addObject:[[SmaatoPost alloc] initWithDictionary:obj]];
        }];

        completion([NSArray arrayWithArray:users], nil);
    }];
}

@end
