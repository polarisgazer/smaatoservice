//
//  SmaatoPostsProviding.h
//  SmaatoService
//
//  Created by Cristian Azov on 26.04.21.
//

#import <Foundation/Foundation.h>
@class SmaatoPost;

typedef void (^PostProviderCompletion)(SmaatoPost * _Nullable post, NSError * _Nullable error);

@protocol SmaatoPostsProviding <NSObject>

NS_ASSUME_NONNULL_BEGIN

- (void)fetchRandomPostWithCompletion:(PostProviderCompletion)completion;
- (BOOL)markAsFavoritePost:(SmaatoPost *)post error:(NSError * _Nullable *)errorPtr;

NS_ASSUME_NONNULL_END

@end
