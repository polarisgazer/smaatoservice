//
//  SmaatoPostProvider.h
//  SmaatoService
//
//  Created by Cristian Azov on 26.04.21.
//

#import <Foundation/Foundation.h>
#import "SmaatoPostProviding.h"
@protocol SmaatoNetworkingProviding;

NS_ASSUME_NONNULL_BEGIN

@interface SmaatoPostProvider : NSObject <SmaatoPostProviding>

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithNetworkingProvider:(id<SmaatoNetworkingProviding>)NetworkingProvider;

@end

NS_ASSUME_NONNULL_END
