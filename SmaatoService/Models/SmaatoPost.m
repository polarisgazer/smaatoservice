//
//  SmaatoPost.m
//  SmaatoService
//
//  Created by Cristian Azov on 26.04.21.
//

#import "SmaatoPost.h"
#import "SmaatoUser.h"

static NSString *const SmaatoPostContentTypeValueImage = @"img";
static NSString *const SmaatoPostContentTypeValueText = @"text";

@implementation SmaatoPost

- (instancetype)initWithDictionary:(NSDictionary<NSString *, id> *)dictionary {
    self = [super init];
    if (self) {
        NSNumber *secondsAgo = dictionary[@"created"];
        _creationDate = [NSDate dateWithTimeIntervalSinceNow:secondsAgo.doubleValue];

        NSString *contentType = (NSString *)[dictionary valueForKeyPath:@"type"];

        if (contentType != nil) {
            if ([[contentType lowercaseString] isEqualToString:SmaatoPostContentTypeValueImage]) {
                _contentType = SmaatoPostContentTypeImage;
            }
            if ([[contentType lowercaseString] isEqualToString:SmaatoPostContentTypeValueText]) {
                _contentType = SmaatoPostContentTypeText;
            }
        } else {
            _contentType = SmaatoPostContentTypeNone;
        }

        NSString *urlString = (NSString *)[dictionary valueForKeyPath:@"data.url"];
        NSString *text = (NSString *)[dictionary valueForKeyPath:@"data.text"];

        if (urlString != nil) {
            _imageURLString = urlString;
        } else if (text != nil) {
            _text = text;
        }
        _user = [[SmaatoUser alloc] initWithDictionary:dictionary[@"user"]];

    }
    return self;
}

#pragma mark - NSCoding
- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        _creationDate = [coder decodeObjectOfClass:[NSDate class] forKey:@"creationDate"];
        _imageURLString = [coder decodeObjectOfClass:[NSString class] forKey:@"imageURLString"];
        _text = [coder decodeObjectOfClass:[NSString class] forKey:@"text"];
        _user = [coder decodeObjectOfClass:[SmaatoUser class] forKey:@"user"];
    }

    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:_creationDate forKey:@"creationDate"];
    [coder encodeObject:_imageURLString forKey:@"imageURLString"];
    [coder encodeObject:_text forKey:@"text"];
    [coder encodeObject:_user forKey:@"user"];
}

+ (BOOL)supportsSecureCoding {
   return YES;
}

@end
