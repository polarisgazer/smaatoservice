//
//  SmaatoPostViewDataModel.m
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import "SmaatoPostViewDataModel.h"
#import "SmaatoPost.h"
#import "SmaatoUser.h"

@implementation SmaatoPostViewDataModel

- (instancetype)initWithPost:(SmaatoPost *)post
{
    self = [super init];
    if (self) {
        [self setupCreationDate:post.creationDate];
        if (post.imageURLString != nil) {
            _imageURL = [NSURL URLWithString:post.imageURLString];
        }
        _text = post.text;
        _name = post.user.name;
        _country = post.user.country;
    }
    return self;
}

- (void)setupCreationDate:(nullable NSDate *)date {
    if (date == nil) {
        return;
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateStyle = NSDateFormatterShortStyle;
    _creationDate = [formatter stringFromDate:date];
}

@end
