//
//  SmaatoPostViewDataModel.h
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import <Foundation/Foundation.h>
@class SmaatoPost;

@interface SmaatoPostViewDataModel : NSObject

@property (nonatomic, strong, nullable) NSString *creationDate;
@property (nonatomic, strong, nullable) NSURL *imageURL;
@property (nonatomic, strong, nullable) NSString *text;
@property (nonatomic, strong, nullable) NSString *name;
@property (nonatomic, strong, nullable) NSString *country;

NS_ASSUME_NONNULL_BEGIN

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithPost:(SmaatoPost *)post;

NS_ASSUME_NONNULL_END

@end
