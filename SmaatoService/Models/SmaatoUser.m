//
//  SmaatoUserInfo.m
//  SmaatoService
//
//  Created by Cristian Azov on 26.04.21.
//

#import "SmaatoUser.h"

@implementation SmaatoUser

- (instancetype)initWithDictionary:(NSDictionary<NSString *, NSString *> *)dictionary {
    self = [super init];
    if (self) {
        _name = dictionary[@"name"];
        _country = dictionary[@"country"];
    }
    return self;
}

#pragma mark - NSCoding
- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        _name = [coder decodeObjectOfClass:[NSString class] forKey:@"name"];
        _country = [coder decodeObjectOfClass:[NSString class] forKey:@"country"];
    }

    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder{
    [coder encodeObject:_name forKey:@"name"];
    [coder encodeObject:_country forKey:@"country"];
}

+ (BOOL)supportsSecureCoding {
   return YES;
}

@end
