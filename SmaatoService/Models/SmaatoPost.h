//
//  SmaatoPost.h
//  SmaatoService
//
//  Created by Cristian Azov on 26.04.21.
//

#import <Foundation/Foundation.h>
@class SmaatoUser;

typedef NS_ENUM(NSInteger, SmaatoPostContentType) {
    SmaatoPostContentTypeNone,
    SmaatoPostContentTypeImage,
    SmaatoPostContentTypeText
};

@interface SmaatoPost : NSObject <NSSecureCoding>

@property (nonatomic, strong, nullable) NSDate *creationDate;
@property (nonatomic, strong, nullable) NSString *imageURLString;
@property (nonatomic, strong, nullable) NSString *text;
@property (nonatomic, strong, nullable) SmaatoUser *user;
@property (nonatomic, assign) SmaatoPostContentType contentType;

NS_ASSUME_NONNULL_BEGIN

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithDictionary:(NSDictionary<NSString *, id> *)dictionary;

NS_ASSUME_NONNULL_END

@end

