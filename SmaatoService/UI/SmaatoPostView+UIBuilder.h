//
//  SmaatoPostView+UIBuilder.h
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import "SmaatoPostView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SmaatoPostView (UIBuilder)

+ (UIStackView *)makeContentStackView;
+ (UILabel *)makeCreationDateLabel;
+ (UILabel *)makeTextLabel;
+ (UIImageView *)makeImageView;
+ (UILabel *)makeNameLabel;
+ (UILabel *)makeCountryLabel;
+ (UIStackView *)makeInfoStackViewWithName:(UILabel *)nameLabel country:(UILabel *)countryLabel;

@end

NS_ASSUME_NONNULL_END
