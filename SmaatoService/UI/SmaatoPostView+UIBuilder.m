//
//  SmaatoPostView+UIBuilder.m
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import "SmaatoPostView+UIBuilder.h"
#import "UIView+Layout.h"

@implementation SmaatoPostView (UIBuilder)

+ (UIStackView *)makeContentStackView {
    UIStackView *stackView = [[UIStackView alloc] init];
    stackView.axis = UIAxisVertical;
    stackView.spacing = 8;
    return stackView;
}

+ (UILabel *)makeCreationDateLabel {
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor grayColor];
    label.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    label.textAlignment = [label isRightToLeftLayout] ? NSTextAlignmentLeft : NSTextAlignmentRight;
    return label;
}

+ (UILabel *)makeTextLabel {
    UILabel *label = [[UILabel alloc] init];
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = 0.6;
    label.textColor = [UIColor darkTextColor];
    label.font = [UIFont preferredFontForTextStyle:UIFontTextStyleTitle1];
    return label;
}

+ (UIImageView *)makeImageView {
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    return imageView;
}

+ (UILabel *)makeNameLabel {
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor lightTextColor];
    label.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption2];
    return label;
}

+ (UILabel *)makeCountryLabel {
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor lightTextColor];
    label.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption2];
    label.textAlignment = [label isRightToLeftLayout] ? NSTextAlignmentLeft : NSTextAlignmentRight;
    return label;
}

+ (UIStackView *)makeInfoStackViewWithName:(UILabel *)nameLabel country:(UILabel *)countryLabel {
    UIStackView *stackView = [[UIStackView alloc] initWithArrangedSubviews:@[nameLabel, countryLabel]];
    stackView.spacing = 8;
    return stackView;
}

@end
