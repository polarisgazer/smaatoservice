//
//  UserView.m
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import "SmaatoPostView.h"
#import "SmaatoUser.h"
#import "PostView+UIBuilder.h"
#import "SmaatoPostViewDataModel.h"
#import "UIImageView+URL.h"

@interface SmaatoPostView ()

@property (nonatomic, strong) UILabel *creationDateLabel;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *countryLabel;
@property (nonatomic, strong) UIStackView *infoStackView;
@property (nonatomic, assign) NSInteger reusableViewIdentifier;

@end

@implementation SmaatoPostView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
    if (self) {
        _creationDateLabel = [SmaatoPostView makeCreationDateLabel];
        _imageView = [SmaatoPostView makeImageView];
        _textLabel = [SmaatoPostView makeTextLabel];
        _nameLabel = [SmaatoPostView makeNameLabel];
        _countryLabel = [SmaatoPostView makeCountryLabel];
        _infoStackView = [SmaatoPostView makeInfoStackViewWithName:_nameLabel country:_countryLabel];

        [self setupUI];
    }
    return self;
}

- (void)updateWithUser:(SmaatoPostViewDataModel *)post reusableViewIdentifier:(NSInteger)identifier {
    _reusableViewIdentifier  = identifier;
    [self updateCreationDate:post.creationDate];
    [self updateImageWithURL:post.imageURL orText:post.text];
    [self updateName:post.name andCountry:post.country];
}

- (void)updateCreationDate:(NSString *)creationDate {
    if (creationDate == nil) {
        [_creationDateLabel setHidden:YES];
    } else {
        _creationDateLabel.text = creationDate;
        [_creationDateLabel setHidden:NO];
    }
}

- (void)updateImageWithURL:(NSURL *)url orText:(NSString *)text {
    if (url == nil) {
        [_imageView setHidden:YES];
    } else {
        NSInteger viewIdentifier = _reusableViewIdentifier;
        __weak typeof(self) weakSelf = self;

        [_imageView fetchImageAtURL:url completion:^(UIImage * _Nonnull image) {
            // Make sure not to update a reused view with obsolete data
            if (viewIdentifier != weakSelf.reusableViewIdentifier) {
                return;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.imageView.image = image;
            });
        }];
        [_imageView setHidden:NO];
    }

    if (text == nil) {
        [_textLabel setHidden:YES];
    } else {
        _textLabel.text = text;
        [_textLabel setHidden:NO];
    }
}

- (void)updateName:(nullable NSString *)name andCountry:(nullable NSString *)country {
    if (name == nil && country == nil) {
        [_infoStackView setHidden:YES];
        return;
    }

    [_infoStackView setHidden:NO];

    if (name == nil) {
        [_nameLabel setHidden:YES];
    } else {
        _nameLabel.text = name;
        [_nameLabel setHidden:NO];
    }

    if (country == nil) {
        [_countryLabel setHidden:YES];
    } else {
        _countryLabel.text = name;
        [_countryLabel setHidden:NO];
    }
}

- (void)setupUI {
    UIStackView *contentStackView = [SmaatoPostView makeContentStackView];
    contentStackView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:contentStackView];

    NSArray<UIView *> *subviews = @[
        _creationDateLabel,
        _imageView,
        _textLabel,
        _infoStackView
    ];

    for (UIView *view in subviews) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
        [contentStackView addArrangedSubview:view];
    }

    [_countryLabel setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];

    NSArray<NSLayoutConstraint *> *constraints = @[
        [contentStackView.topAnchor constraintEqualToAnchor:self.layoutMarginsGuide.topAnchor],
        [contentStackView.leadingAnchor constraintEqualToAnchor:self.layoutMarginsGuide.leadingAnchor],
        [contentStackView.trailingAnchor constraintEqualToAnchor:self.layoutMarginsGuide.trailingAnchor],
        [contentStackView.bottomAnchor constraintEqualToAnchor:self.layoutMarginsGuide.bottomAnchor],

        [_imageView.heightAnchor constraintEqualToConstant:100]
    ];

    [NSLayoutConstraint activateConstraints:constraints];
}

@end
