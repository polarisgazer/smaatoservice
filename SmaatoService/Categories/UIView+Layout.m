//
//  UIView+Layout.m
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import "UIView+Layout.h"

@implementation UIView (Layout)

- (BOOL)isRightToLeftLayout {
    return [UIView userInterfaceLayoutDirectionForSemanticContentAttribute:self.semanticContentAttribute] == UIUserInterfaceLayoutDirectionRightToLeft;
}
@end
