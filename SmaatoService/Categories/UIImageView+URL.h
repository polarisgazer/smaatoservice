//
//  UIImageView+URL.h
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImageView (URL)

@property (nonatomic, strong) NSURLSessionDataTask *dataTask;

- (void)fetchImageAtURL:(nonnull NSURL *)url completion: (void (^ _Nullable)(UIImage *image))completion;

@end

NS_ASSUME_NONNULL_END
