//
//  NSString+SHA256.m
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import "NSString+SHA256.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (SHA256)

- (NSString *)sha256 {

    const char *utf8Str = [self UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(utf8Str, (CC_LONG)strlen(utf8Str), result);

    NSMutableString *encodedStr = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
        [encodedStr appendFormat:@"%02x",result[i]];
    }
    return encodedStr;
}

@end
