//
//  UIView+Layout.h
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Layout)

- (BOOL)isRightToLeftLayout;

@end

NS_ASSUME_NONNULL_END
