//
//  NSString+SHA256.h
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import <Foundation/Foundation.h>

@interface NSString (SHA256)

- (nonnull NSString *)sha256;

@end
