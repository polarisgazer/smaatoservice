//
//  UIImageView+URL.m
//  SmaatoService
//
//  Created by Cristian Azov on 27.04.21.
//

#import "UIImageView+URL.h"
#import <objc/runtime.h>
#import "SmaatoImageCache+Singleton.h"

@implementation UIImageView (URL)

- (void)fetchImageAtURL:(nonnull NSURL *)url completion: (void (^ _Nullable)(UIImage *image))completion {

    [self.dataTask cancel];

    UIImage *cachedImage  = [SmaatoImageCache.sharedImageCache cachedImageWithURL:url];
    if (cachedImage != nil) {
        completion(cachedImage);
        return;
    }

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];

    NSURLSessionDataTask *dataTask =
    [session dataTaskWithURL:url
           completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data == nil) {
            completion(nil);
            return;
        }

        UIImage *image = [UIImage imageWithData:data];
        if (image != nil) {
            [SmaatoImageCache.sharedImageCache cacheImage:image withURL:url];
        }
        completion(image);
    }];

    [dataTask resume];
    self.dataTask = dataTask;
}

- (void)setDataTask:(NSURLSessionDataTask *)newDataTask {
     objc_setAssociatedObject(self, @selector(dataTask), newDataTask, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSURLSessionDataTask *)dataTask {
    return objc_getAssociatedObject(self, @selector(dataTask));
}

@end
